#include "Methods.h"

/////////////////////////////////////////////////////////////////////////////
//                              Function Definitions                       //
/////////////////////////////////////////////////////////////////////////////

void getTemperatures(feedback_t * feedback)
{

	//Put code here

}

void controlFans(feedback_t * feedback)
{

	//Put code here

}

void TAPfeedbackCapture(feedback_t * feedback)
{

	//Put code here

}

// Handles transitions between modes and sets trajectory and propulsion commands accordingly
void ModeTransitionControlSequencer(
	reference_t * reference,
	feedback_t * feedback,
	command_t * commands
)
{

	//Put code here

}

// Controls Rover Trajectory
void TrajectoryControlSystem(
	feedback_t * feedback,
	command_t * commands
)
{

	//Put code here

}

// Controls Rover Propulsion
void PropulsionControlSystem(
	feedback_t * feedback,
	command_t * commands
)
{

	//Put code here

}

// Check Fault Flags
void checkAndActionOnFaults(flags_t * flags)
{

	//Put code here

}










