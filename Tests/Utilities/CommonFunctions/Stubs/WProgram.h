#ifndef WProgram_h
#define WProgram_h

#include "stdbool.h"
#include "stdint.h"

static uint16_t Register_Value = 12;

uint16_t analogWriteResVal();

void MimicAnalogPrecision(uint16_t Mimic_Value);


#endif // WProgram_h
